if (typeof jQuery === "undefined") {
  throw "Node requires jquery module to be loaded";
}

var magicNode = {
  Node: function(){
    this._node;

    function initWithSelector( selector ){
      this._node = $(selector).first();
      return this;
    }
    function initWithElement( element ){
      this._node = element;
      return this;
    }

    function firstChild() {
      if( this._node.children().first().length == 0){
        return null;
      } else {
        return new magicNode.Node().initWithElement( this._node.children().first() );
      }
    }

    function nextSibling() {
      if( this._node.next().length == 0) {
        return null;
      } else {
        return new magicNode.Node().initWithElement( this._node.next() );
      }
    }

    $.extend(this, {
      "initWithSelector": initWithSelector,
      "initWithElement": initWithElement,
      "firstChild": firstChild,
      "nextSibling": nextSibling
    });

    return this;
  }
};
