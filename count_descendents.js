/**
 * A simple function which returns the number of descendents of an html element
 * @param {Node}      node    A Node object to count the descendents of
 *
 **/
function countDescendents( node, count=0 ) {
  //console.log( "count: " + count + ", node: ", node._node );

  // If only 1 argument this is the starting node
  var starting_node = arguments.length == 1

  var child = node.firstChild();
  // I've made the assumption that firstChild returns null if no children are present
  if( child ){
    count += 1;
    count = countDescendents( child, count );
  }

  var sibling = node.nextSibling();
  // Dont look for siblings if none exist or this is the starting node
  if( sibling && !starting_node ){
    count += 1;
    count = countDescendents( sibling, count );
  }

  return count;
}
